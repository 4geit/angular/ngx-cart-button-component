import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { NgxCartButtonComponent } from './ngx-cart-button.component';
import { NgxMaterialModule } from '@4geit/ngx-material-module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    NgxMaterialModule,
  ],
  declarations: [
    NgxCartButtonComponent
  ],
  exports: [
    NgxCartButtonComponent
  ]
})
export class NgxCartButtonModule { }
