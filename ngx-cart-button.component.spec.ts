import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgxCartButtonComponent } from './ngx-cart-button.component';

describe('cart-button', () => {
  let component: cart-button;
  let fixture: ComponentFixture<cart-button>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ cart-button ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(cart-button);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
