import { Component, OnInit, Input, Optional } from '@angular/core';

import { NgxCartItemsService } from '@4geit/ngx-cart-items-service';

@Component({
  selector: 'ngx-cart-button',
  template: require('pug-loader!./ngx-cart-button.component.pug')(),
  styleUrls: ['./ngx-cart-button.component.scss']
})
export class NgxCartButtonComponent implements OnInit {

  @Input() add = false;
  @Input() item: any;

  items: any;

  constructor(
    private cartItemsService: NgxCartItemsService
  ) { }

  ngOnInit() {
    this.items = this.cartItemsService.items;
  }

}
